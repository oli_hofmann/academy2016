namespace Garaio.Academy.Data
{
    public class TaskItem
    {
        public int Id {get; set;}
        public string Name { get; set;}
        public string Notes {get; set;}
        public bool Done {get; set;}
    }
}