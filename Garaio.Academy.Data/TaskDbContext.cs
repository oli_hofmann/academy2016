using Microsoft.EntityFrameworkCore;

namespace Garaio.Academy.Data
{
    public class TaskDbContext : DbContext
    {
        public DbSet<TaskItem> TaskItems {get; set;}

        public TaskDbContext(DbContextOptions<TaskDbContext> options) : base(options)
		{}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskItem>().HasKey(t => t.Id);
        }
    }   
}