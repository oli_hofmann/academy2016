using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Garaio.Academy.Core;
using Garaio.Academy.Data;
using Garaio.Academy.Web.Models;
using Garaio.Academy.Core.Enums;
using Garaio.Academy.Core.Options;
using Microsoft.Extensions.Options;

namespace Garaio.Academy.Web.Controllers
{
    public class HomeController : Controller
    {
        private ITaskRepository _taskRepository;

        private IOptions<TaskGridSettings> _settings;

        public HomeController(ITaskRepository taskRepository, IOptions<TaskGridSettings> settings)
        {
            if (taskRepository == null)
            {
                throw new ArgumentException(nameof(taskRepository));
            }

            this._taskRepository = taskRepository;
            this._settings = settings;
        }

        [HttpGet]
        public IActionResult Index(StatusMessageIds? message = null)
        {
            ViewData["AlertType"] = message.ToString().Contains("Success") ? "success" : "danger";
            ViewData["StatusMessage"] =
				message == StatusMessageIds.CreateSuccess ? "Task erfolgreich gespeichert"
				: message == StatusMessageIds.Error ? "Task konnte nicht gespeichert werden"
				: string.Empty;

            IQueryable<TaskItem> tasks = this._taskRepository.GetAll().Take(this._settings.Value.ItemCount);
            IQueryable<TaskViewModel> model = tasks.Select(t =>
                new TaskViewModel { Id = t.Id, Name = t.Name, Notes = t.Notes, Done = t.Done });
            
            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            TaskViewModel model = new TaskViewModel();

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(int taskId)
        {
            TaskItem task = this._taskRepository.GetById(taskId);
            TaskViewModel model = new TaskViewModel
            {
                Id = task.Id,
                Name = task.Name,
                Notes = task.Notes,
                Done = task.Done
            };

            return View(model);
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
