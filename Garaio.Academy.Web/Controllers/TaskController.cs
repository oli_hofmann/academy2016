using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Garaio.Academy.Core;
using Garaio.Academy.Data;
using Garaio.Academy.Web.Models;
using Garaio.Academy.Core.Enums;

namespace Garaio.Academy.Web.Controllers
{
    public class TaskController : Controller
    {
        private ITaskRepository _taskRepository;

        public TaskController(ITaskRepository taskRepository)
        {
            if (taskRepository == null)
            {
                throw new ArgumentException(nameof(taskRepository));
            }

            this._taskRepository = taskRepository;
        }

        [HttpPost]
        public IActionResult CreateTask(TaskViewModel model)
        {
            if (model == null)
            {
                throw new ArgumentException(nameof(model));
            }

            try
            {
                if (ModelState.IsValid)
                {
                    TaskItem task = new TaskItem
                    {
                        Name = model.Name,
                        Notes = model.Notes
                    };

                    this._taskRepository.Add(task);
                    this._taskRepository.SaveChanges();
                }   
            }
            catch (Exception)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home", new { Message = StatusMessageIds.Error });
            }

            return RedirectToAction(nameof(HomeController.Index), "Home", new { Message = StatusMessageIds.CreateSuccess });
        }

        [HttpPost]
        public IActionResult EditTask(TaskViewModel model)
        {
            if (model == null)
            {
                throw new ArgumentException(nameof(model));
            }

            try
            {
                if (ModelState.IsValid)
                {
                    TaskItem task = this._taskRepository.GetById(model.Id);
                    task.Name = model.Name;
                    task.Notes = model.Notes;
                    task.Done = model.Done;

                    this._taskRepository.Update(task);
                    this._taskRepository.SaveChanges();
                }   
            }
            catch (Exception)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home", new { Message = StatusMessageIds.Error });
            }

            return RedirectToAction(nameof(HomeController.Index), "Home", new { Message = StatusMessageIds.CreateSuccess });
        }
    }
}