using System.ComponentModel.DataAnnotations;

namespace Garaio.Academy.Web.Models
{
    public class TaskViewModel
    {
        public int Id { get; set;}
        
        [Display(Name = "Name des Tasks")]
        [Required(ErrorMessage = "Der Name ist ein Pflichtfeld")]
        public string Name { get; set; }
        
        [Display(Name = "Notizen des Tasks")]
        [Required(ErrorMessage = "Die Notizen sind ein Pflichtfeld")]
        public string Notes { get; set;}
        
        [Display(Name = "Task abgeschlossen")]
        public bool Done { get; set;}
    }
}