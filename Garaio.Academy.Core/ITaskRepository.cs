using Garaio.Academy.Data;

namespace Garaio.Academy.Core
{
    public interface ITaskRepository : IRepository<TaskItem>
    {
        TaskItem GetById(int id);
    }
}