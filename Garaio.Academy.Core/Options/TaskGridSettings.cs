namespace Garaio.Academy.Core.Options
{
    public class TaskGridSettings
    {
        public int ItemCount {get; set; } = 100;
    }
}