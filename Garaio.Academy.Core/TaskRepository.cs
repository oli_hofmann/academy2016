using System;
using System.Linq;

using Garaio.Academy.Data;

namespace Garaio.Academy.Core
{
    public class TaskRepository : ITaskRepository
    {
        private TaskDbContext _dbContext;

        public TaskRepository(TaskDbContext dbContext)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException(nameof(dbContext));
            }

            this._dbContext = dbContext;
        }

        public TaskItem Add(TaskItem item)
        {
            if (item == null)
            {
                throw new ArgumentException(nameof(item));
            }

            return this._dbContext.Add(item).Entity;
        }

        public void Delete(TaskItem item)
        {
            throw new NotImplementedException();
        }

        public IQueryable<TaskItem> GetAll()
        {
            return this._dbContext.TaskItems;
        }

        public TaskItem GetById(int id)
        {
            return this._dbContext.TaskItems.SingleOrDefault(t => t.Id.Equals(id));
        }

        public void SaveChanges()
        {
            this._dbContext.SaveChanges();
        }

        public TaskItem Update(TaskItem item)
        {
            if (item == null)
            {
                throw new ArgumentException(nameof(item));
            }

            this._dbContext.Attach(item);
            return this._dbContext.Update(item).Entity;
        }
    }
}