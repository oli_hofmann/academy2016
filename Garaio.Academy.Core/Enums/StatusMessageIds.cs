namespace Garaio.Academy.Core.Enums
{
    public enum StatusMessageIds
    {
        CreateSuccess,
		EditSuccess,
		Error
    }
}