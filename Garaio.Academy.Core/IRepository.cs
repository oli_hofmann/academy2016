using System.Linq;

namespace Garaio.Academy.Core
{
    public interface IRepository<T> where T : class
    {
        T Add(T item);
        T Update(T item);
        void Delete(T item);
        IQueryable<T> GetAll();
        void SaveChanges();
    }
}