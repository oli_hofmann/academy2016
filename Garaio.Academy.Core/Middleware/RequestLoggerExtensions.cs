using Microsoft.AspNetCore.Builder;

namespace Garaio.Academy.Core.Middleware
{
    public static class RequestLoggerExtensions
    {
        public static IApplicationBuilder UseRequestLogger(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestLogger>();
        }
    }
}