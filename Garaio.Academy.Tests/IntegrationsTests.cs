using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

using Xunit;

using Garaio.Academy.Web;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net;
using System;

namespace Garaio.Academy.Tests
{
    public class IntegrationTests : IDisposable
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public IntegrationTests()
        {
            var host = new WebHostBuilder()
                .UseEnvironment("Development")
                .UseKestrel()
                .UseContentRoot("../../../../Garaio.Academy.Web")
                .UseIISIntegration()
                .UseStartup<Startup>();

            this._server = new TestServer(host);
            this._client = _server.CreateClient();
        }

        [Fact]
        public async Task ReturnSuccessStatusCode()
        {
            var response = await this._client.GetAsync("/");
            HttpResponseMessage responseMessage = response.EnsureSuccessStatusCode();
            
            Assert.True(responseMessage.IsSuccessStatusCode);
        }

        [Fact]
        public async Task CreateTask_ReturnRedirect()
        {
            Dictionary<string, string> pairs = new Dictionary<string,string>();
            pairs.Add("Name", "Test Task");
            pairs.Add("Notes", "Test Notes");
            
            FormUrlEncodedContent formContent = new FormUrlEncodedContent(pairs);
            HttpResponseMessage responseMessage = await this._client.PostAsync("/Task/CreateTask", formContent);
            
            Assert.Equal(HttpStatusCode.Redirect, responseMessage.StatusCode);
        }

        [Fact]
        public async Task EditTask_ReturnSuccessStatusCode()
        {
            HttpResponseMessage responseMessage = await this._client.GetAsync("Home/Edit?taskid=1");
            Assert.True(responseMessage.IsSuccessStatusCode);
        }

        public void Dispose()
        {
            this._server.Dispose();
            this._client.Dispose();
        }

        // [Fact]
        // public async Task CreateTask_ReturnSuccess()
        // {
        //     Dictionary<string, string> pairs = new Dictionary<string,string>();
        //     pairs.Add("Name", "Test Task");
        //     pairs.Add("Notes", "Test Notes");

        //     FormUrlEncodedContent formContent = new FormUrlEncodedContent(pairs);
        //     HttpResponseMessage responseMessage = await this._client.PostAsync("/Task/CreateTask", formContent);

        //     var responseString = await responseMessage.Content.ReadAsStringAsync();

        //     Assert.Contains("erfolgreich", responseString);
        // }
    }
}