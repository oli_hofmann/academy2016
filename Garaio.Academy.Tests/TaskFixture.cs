using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using Bogus;
using Xunit;

using Garaio.Academy.Data;
using Garaio.Academy.Core;

namespace Garaio.Academy.Tests
{
    public class TaskFixture : IDisposable
    {
        private TaskDbContext _dbContext;

        private ITaskRepository _taskRepository;
        
        public TaskFixture()
        {
            IServiceCollection serviceCollection = new ServiceCollection();
			serviceCollection.AddEntityFrameworkInMemoryDatabase().AddDbContext<TaskDbContext>(options => options.UseInMemoryDatabase());
			IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();

            this._dbContext = serviceProvider.GetService<TaskDbContext>();
            Faker<TaskItem> faker = new Faker<TaskItem>()
                .RuleFor(t => t.Name, f => f.Lorem.Sentence(2))
                .RuleFor(t => t.Notes, f => f.Lorem.Sentence(4));

            this._dbContext.AddRange(faker.Generate(25));
            this._dbContext.SaveChanges();

            this._taskRepository = new TaskRepository(this._dbContext);
        }

        public void Dispose()
        {
            this._dbContext.RemoveRange(this._dbContext.TaskItems);
            this._dbContext.SaveChanges();

            this._dbContext.Dispose();
        }

        [Fact]
        public void GetAll_ReturnAllItems()
        {
            IEnumerable<TaskItem> tasks = this._taskRepository.GetAll();
            Assert.Equal(25, tasks.Count());
        }

        [Fact]
        public void GetById_ReturnCorrectTaskItem()
        {
            TaskItem task = this._taskRepository.GetById(3);
            Assert.NotNull(task);
        }

        [Fact]
        public void Update_SetNewName_ReturnCorrectName()
        {
            TaskItem task = this._taskRepository.GetById(3);
            task.Name = "Neuer Name";

            this._taskRepository.Update(task);
            Exception ex = Record.Exception(() => this._taskRepository.SaveChanges());

            Assert.Null(ex);
        }
    }
}
